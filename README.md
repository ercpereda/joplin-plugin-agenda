# Agenda

An agenda/calendar/schedule panel plugin for joplin that shows all uncompleted to-dos with a due date

## Screenshots
### Main Interface
![Screenshot1](docs/Screenshot1.png)

### The Agenda Panel
![Screenshot2](docs/Screenshot2.png)

## Development
* Download Repo
* Run `npm install`
* Modify code in `/src`
* Update Metadata in `/src/manifest.json` and `/package.json`
* Build plugin with `npm run dist`
* Update the plugin framework with `npm run update`
* Publish using `npm publish`
